
#define CM_FILE_NEW     0
#define CM_FILE_SAVEAS	1
#define CM_FILE_EXIT	2
#define CM_FILE_OPEN	3

#define CM_EDIT_UNDO    11 
#define CM_EDIT_REDO    12
#define CM_EDIT_CUT     13
#define CM_EDIT_COPY    14
#define CM_EDIT_PASTE   15

#define CM_SEARCH_FIND  21
#define CM_SEARCH_FINDinFILES   22
#define CM_SEARCH_SEARCHAGAIN   23
#define CM_SEARCH_GOTOFUNCTION  24
#define CM_SEARCH_GOTOLINE      25

#define CM_PROJECT_NEWFILE      31
#define CM_PROJECT_ADDPROJECT   32
#define CM_PROJECT_REMOVEPROJECT  33
#define CM_PROJECT_PROJECTOPTIONS 34

#define CM_EXECUTE_COMPILE        41       
#define CM_EXECUTE_COMPILERUN     42
#define CM_EXECUTE_RUN            43



#define CM_ABOUT        100



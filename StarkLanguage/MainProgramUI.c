#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <stdio.h>
#include "resource.h"

const char g_szClassName[] = "Stark::Language";

HINSTANCE hInstance;
//CREATE WINDOW OBJECTS
HWND hMainWndow, hEdit, hErrorList, hToolBar, hTab;

//LOAD FILE
BOOL LoadToEditTextFile(HWND hEdit, LPCTSTR szTextFileName) {
		HANDLE hTextFileHandler;
		BOOL b_LoadingFileSuccess = FALSE;

		hTextFileHandler = CreateFile(szTextFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		if(hTextFileHandler != INVALID_HANDLE_VALUE) {
			DWORD dwFileSize;
			dwFileSize = GetFileSize(hTextFileHandler, NULL);
			if(dwFileSize != 0xFFFFFFFF) {
				LPSTR szFileText;
				szFileText = (LPSTR)GlobalAlloc(GPTR, dwFileSize + 1);
				if(szFileText != NULL) {
					DWORD dwRead;
					if(ReadFile(hTextFileHandler, szFileText, dwFileSize, &dwRead, NULL)){
						szFileText[dwFileSize] = 0;
						if(SetWindowText(hEdit, szFileText))
							b_LoadingFileSuccess = TRUE;
					}
					GlobalFree(szFileText);
				}
			}
			CloseHandle(hTextFileHandler);
		}
		return b_LoadingFileSuccess;
	}
//SAVE TEXT
	BOOL SaveTextFile(HWND hEdit, LPCTSTR szFileName) {
		HANDLE hTextFileHandler;
		BOOL b_SavingFileSuccess = FALSE;

		hTextFileHandler = CreateFile(szFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if(hTextFileHandler != INVALID_HANDLE_VALUE) {
			DWORD dwTextLength;
			dwTextLength = GetWindowTextLength(hEdit);
			if(dwTextLength > 0) {
				LPSTR szText;
				DWORD dwBufferSize = dwTextLength + 1;
				szText = (LPSTR)GlobalAlloc(GPTR, dwBufferSize);
				if(szText != NULL) {
					if(GetWindowText(hEdit, szText, dwBufferSize)) {
						DWORD dwWritten;
						if(WriteFile(hTextFileHandler, szText, dwTextLength, &dwWritten, NULL))
							b_SavingFileSuccess = TRUE;
					}
					GlobalFree(szText);
				}
			}
			CloseHandle(hTextFileHandler);
		}
		return b_SavingFileSuccess;
	}
//OPEN FILE
	void DoFileOpen(HWND hMainWindow) {
		OPENFILENAME stark_FileName;
		ZeroMemory(&stark_FileName, sizeof(stark_FileName));
		char szFileName[MAX_PATH] = "";

		stark_FileName.lStructSize = sizeof(OPENFILENAME);
		stark_FileName.hwndOwner = hMainWindow;
		stark_FileName.lpstrFilter = "Stark source file (*.stk)\0*.stk\0";
		stark_FileName.lpstrFile = szFileName;
		stark_FileName.nMaxFile = MAX_PATH;
		stark_FileName.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
		stark_FileName.lpstrDefExt = "stk";

		if (GetOpenFileName(&stark_FileName)) {
			HWND hEdit= GetDlgItem(hMainWindow, IDM_TEXT);
			LoadToEditTextFile(hEdit, szFileName);
			if (strcmp(szFileName, "") != 0) {
				SetWindowText(hMainWindow, strcat(szFileName, " - Stark::Language v1.00"));
			}
		}
	}
//SAVE AS
	void DoFileSaveAs(HWND hMainWindow){
		OPENFILENAME stark_FileName;
		ZeroMemory(&stark_FileName, sizeof(stark_FileName));
		char szFileName[MAX_PATH] = "";

		stark_FileName.lStructSize = sizeof(OPENFILENAME);
		stark_FileName.hwndOwner = hMainWindow;
		stark_FileName.lpstrFilter = "Stark source file (*.stk)\0*.stk\0";
		stark_FileName.lpstrFile = szFileName;
		stark_FileName.nMaxFile = MAX_PATH;
		stark_FileName.lpstrDefExt = "stk";
		stark_FileName.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;

		if(GetSaveFileName(&stark_FileName)) {
			HWND hEdit = GetDlgItem(hMainWindow, IDM_TEXT);
			SaveTextFile(hEdit, szFileName);
		}
	}
//CALLBACK PROCEDURE
LRESULT CALLBACK WndProc(HWND hMainWindow, UINT Message, WPARAM wParam, LPARAM lParam)
{
    static HBITMAP hBitmapBackground;
        HDC hdc, hdcMember;
        PAINTSTRUCT pStruct;
        BITMAP bitmapBackground;
        HGDIOBJ oldBitmap;
	switch(Message)
	{
		case WM_CREATE:
        {

            //ADD ICON FOR THE MAIN WINDOW
			HICON hIcon;
            hIcon = (HICON)LoadImage(NULL, "Shazam.ico", IMAGE_ICON, 128, 128, LR_LOADFROMFILE);
                // ICON SHOULD BE 128x128 PIXELS
                if(hIcon)
                {
                    SendMessage(hMainWindow, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
                }
                else
                {
                    MessageBox(hMainWindow, "Could not load large icon! Is it in the current working directory?", "Error", MB_OK | MB_ICONERROR);
                }

            //CREATE TOOLBAR
            TBADDBITMAP tbAddBitmap;         //BITMAP STRUCTURE
            static TBBUTTON tbButtons[6];    //BUTTON STRUCTURE

            hToolBar = CreateWindowEx(0,TOOLBARCLASSNAME, (LPSTR) NULL,
                    WS_CHILD | TBSTYLE_TOOLTIPS,0, 0, 0, 0,
                    hMainWindow, (HMENU) TOOLBAR_ID, hInstance, NULL);

            SendMessage(hToolBar, TB_BUTTONSTRUCTSIZE,(WPARAM) sizeof(TBBUTTON), 0);

            HBITMAP hBitmapToolbar = (HBITMAP) LoadImage(NULL, "tool.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);   //LOADS BITMAP HANDLE
            BITMAP bitmapToolbar;
            GetObject(hBitmapToolbar, sizeof(BITMAP), &bitmapToolbar);                                                      //FILL BITMAP STRUCTURE

            SendMessage(hToolBar, TB_SETBITMAPSIZE, 0, (LPARAM) MAKELONG(bitmapToolbar.bmWidth/6, bitmapToolbar.bmHeight));   //SIZE FOR EACH BITMAP
            SendMessage(hToolBar, TB_SETBUTTONSIZE, 0,(LPARAM) MAKELONG(bitmapToolbar.bmWidth/6, bitmapToolbar.bmHeight));    //SIZE FOR EACH BUTTON
            SendMessage(hToolBar, TB_AUTOSIZE, 0, 0);

            tbAddBitmap.hInst = NULL;
            tbAddBitmap.nID = (int)(HBITMAP)hBitmapToolbar;
            SendMessage(hToolBar, TB_ADDBITMAP, 6, (LPARAM)&tbAddBitmap);

            ZeroMemory(&tbButtons, sizeof(TBBUTTON));
            int intIndex;
              for (intIndex=0; intIndex<6; intIndex++)
              {
                tbButtons[intIndex].iBitmap = intIndex;                 //INDEX OF EACH BITMAP
                tbButtons[intIndex].fsState = TBSTATE_ENABLED;          //BITMAP STATE
                tbButtons[intIndex].fsStyle = TBSTYLE_BUTTON;           //BITMAP STYLE
                tbButtons[intIndex].idCommand = (1100 + intIndex);      //IDENTIFIER FOR EACH INDEX OF BITMAP
                tbButtons[intIndex].dwData = 0;
                tbButtons[intIndex].iString = 0;
              }

            SendMessage(hToolBar, TB_ADDBUTTONS, 6, (LONG) &tbButtons); //ADD BUTTONS
            ShowWindow(hToolBar, SW_SHOW);

            //CREATE FONT
            HFONT hFont = CreateFont(18,0,0,0,0,FALSE, FALSE, FALSE, 1,400, 0, 0, 0, ("Segoe UI"));
            HFONT hFont2 = CreateFont(18,0,0,0,0,FALSE, FALSE, FALSE, 1,400, 0, 0, 0, ("Century Gothic"));

            //CREATE MENU BAR AND MENU ITEMS
            HMENU hMenu, hSubMenu;
			hMenu = CreateMenu();

            //MENU FILE
			hSubMenu = CreatePopupMenu();
			AppendMenu(hSubMenu, MF_STRING, SUBMENU_FILE_NEW, "&New...");
			AppendMenu(hSubMenu, MF_STRING, SUBMENU_FILE_OPEN, "&Open...");
			AppendMenu(hSubMenu, MF_STRING, SUBMENU_FILE_SAVE, "&Save");
			AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, "&File");

            //MENU EXECUTE
			hSubMenu = CreatePopupMenu();
			AppendMenu(hSubMenu, MF_STRING, SUBMENU_FILE_RUN_LEXICAL, "&Lexical Analyzer...");
			AppendMenu(hSubMenu, MF_STRING, SUBMENU_FILE_RUN_SYNTAX, "&Syntax Analyzer..");
			AppendMenu(hSubMenu, MF_STRING, SUBMENU_FILE_RUN_SEMANTIC, "Semantic &Analyzer..");
			AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, "&Execute");

            //MENU ABOUT
			hSubMenu = CreatePopupMenu();
			AppendMenu(hSubMenu, MF_STRING, SUBMENU_FILE_ABOUT, "&About Stark::Language");
			AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, "&About");

			SetMenu(hMainWindow, hMenu);

		    //CREATE TEXTAREA
            hEdit   = CreateWindowEx(0,"Edit","//TODO LOGIC CODE HERE",
					0 | WS_VISIBLE | WS_CHILD | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE
                    | ES_AUTOVSCROLL | ES_AUTOHSCROLL, 150, 34, 1130, 556,
                    hMainWindow, (HMENU)IDM_TEXT, GetModuleHandle(NULL), NULL);

            SendMessage(hEdit, WM_SETFONT, (WPARAM)hFont, FALSE);

            //CREATE ERROR LIST
        /*    hErrorList   = CreateWindowEx(
					WS_EX_CLIENTEDGE,
					"Edit",
					"<DETECTED ERRORS IN YOUR CODE>",
					0 | WS_VISIBLE | WS_CHILD | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL,
                    0, 590,
                    1300, 100, hMainWindow, (HMENU)IDM_ERRORLIST, GetModuleHandle(NULL), NULL );

            SendMessage(hErrorList, WM_SETFONT, (WPARAM)hFont, FALSE); */

            hTab = CreateWindowEx(WS_EX_APPWINDOW, "SysTabControl32", "",
                    WS_CHILD|WS_CLIPSIBLINGS|WS_VISIBLE| WS_EX_APPWINDOW |  WS_EX_LEFTSCROLLBAR,
                    0, 590, 1300, 200, hMainWindow, NULL, hInstance, NULL);

            TCITEM tcItem;
            tcItem.mask = TCIF_TEXT;
            tcItem.pszText = "Build Messages";
            TabCtrl_InsertItem(hTab, 0, &tcItem);

            tcItem.mask = TCIF_TEXT;
            tcItem.pszText = "Build Log";
            TabCtrl_InsertItem(hTab, 1, &tcItem);

            tcItem.mask = TCIF_TEXT;
            tcItem.pszText = "Debugger";
            TabCtrl_InsertItem(hTab, 2, &tcItem);

            tcItem.mask = TCIF_TEXT;
            tcItem.pszText = "Stark::Language";
            TabCtrl_InsertItem(hTab, 3, &tcItem);

            TabCtrl_SetCurSel(hTab, 0);

            //LOAD BMP FILE AND ADD TO BITMAP STRUCTURE
            hBitmapBackground = (HBITMAP) LoadImage(NULL, "back.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

            if (hBitmapToolbar == NULL) {
                    MessageBox(hMainWindow, "Failed to load image", "Error", MB_OK);
            }





            SendMessage(hTab, WM_SETFONT, (WPARAM)hFont2, FALSE);
            //MAIN WINDOW POSITION
            SetWindowPos(hMainWindow, HWND_TOP, 0, 0, SW_NORMAL, SW_NORMAL, SW_SHOWMAXIMIZED);
		}
		break;

		case WM_SIZE:
            SendMessage(hToolBar, TB_AUTOSIZE, 0, 0);
        break;

		case WM_COMMAND:
            //EVENTS FOR MENU ITEMS
			switch(LOWORD(wParam))
			{
				case SUBMENU_FILE_NEW:
				    SetDlgItemText(hMainWindow, IDM_TEXT, "//TODO LOGIC CODE HERE");
				    SetDlgItemText(hMainWindow, IDM_ERRORLIST, "<DETECTED ERRORS IN YOUR PROGRAM>");
				    SetWindowText(hMainWindow, "Untitled - Stark::Language v1.5.3");
				break;
				case SUBMENU_FILE_OPEN:
                    DoFileOpen(hMainWindow);
				break;
				case SUBMENU_FILE_SAVE:
				    DoFileSaveAs(hMainWindow);
				break;
				case SUBMENU_FILE_RUN_LEXICAL:
					MessageBox(hMainWindow, "Executing ... Please wait ...", "BUILD AND RUN", MB_OK | MB_ICONINFORMATION);
				break;
				case SUBMENU_FILE_RUN_SYNTAX:
					MessageBox(hMainWindow, "Executing ... Please wait ...", "BUILD AND RUN", MB_OK | MB_ICONINFORMATION);
				break;
				case SUBMENU_FILE_RUN_SEMANTIC:
					MessageBox(hMainWindow, "Executing ... Please wait ...", "BUILD AND RUN", MB_OK | MB_ICONINFORMATION);
				break;
				case SUBMENU_FILE_ABOUT:
					MessageBox(hMainWindow, "This is a sample description of the compiler!", "About the Compiler", MB_OK | MB_ICONINFORMATION);
				break;
				case 1100: // NEW FILE
                    SetDlgItemText(hMainWindow, IDM_TEXT, "//TODO LOGIC CODE HERE");
				    SetDlgItemText(hMainWindow, IDM_ERRORLIST, "<DETECTED ERRORS IN YOUR PROGRAM>");
				    SetWindowText(hMainWindow, "Untitled - Stark::Language v1.5.3");
                break;
                case 1101: // OPEN FILE
                    DoFileOpen(hMainWindow);
                break;
                case 1102: // SAVE FILE
                    DoFileSaveAs(hMainWindow);
                break;
			}
        break;

        case WM_NOTIFY: //CREATE TOOLTIP TEXTS FOR THE TOOLBAR BUTTONS
              switch (((LPNMHDR) lParam)->code) //SWITCH - CASE FOR THE VALUE  CODE IN (LPNMHDR) lParam
              {

                case TTN_NEEDTEXT:
                {
                    LPTOOLTIPTEXT lpToolTipText;               //TOOLTIP STRUCTURE
                    lpToolTipText = (LPTOOLTIPTEXT) lParam;    //ADD VALUES TO STRUCTURE
                    lpToolTipText->hinst = hInstance;            //SET THE VALUE OF hinst MEMBER IN THE STRUCTURE TO hInstance

                    switch(lpToolTipText->hdr.idFrom)
                    {
                        case 1100:
                          lpToolTipText->lpszText = "New File";
                          break;
                        case 1101:
                          lpToolTipText->lpszText = "Open File";
                          break;
                        case 1102:
                          lpToolTipText->lpszText = "Save File";
                          break;
                        case 1103:
                          lpToolTipText->lpszText = "Lexical Analyzer";
                          break;
                        case 1104:
                          lpToolTipText->lpszText = "Syntax Analyzer";
                          break;
                        case 1105:
                          lpToolTipText->lpszText = "Semantic Analyzer";
                          break;
                      }
                }
            }
        break;
        case WM_PAINT:
            //ADD THE BITMAP BACKGROUND
            hdc = BeginPaint(hMainWindow, &pStruct);
            hdcMember = CreateCompatibleDC(hdc);
            oldBitmap = SelectObject(hdcMember, hBitmapBackground);
            GetObject(hBitmapBackground, sizeof(bitmapBackground), &bitmapBackground);
            BitBlt(hdc, 0, 33, bitmapBackground.bmWidth, bitmapBackground.bmHeight,
            hdcMember, 0, 0, SRCCOPY);
            SelectObject(hdcMember, oldBitmap);
            DeleteDC(hdcMember);
            EndPaint(hMainWindow, &pStruct);
        break;
		case WM_CLOSE:
			DestroyWindow(hMainWindow);
		break;
		case WM_DESTROY:
			PostQuitMessage(0);
		break;
		default:
			return DefWindowProc(hMainWindow, Message, wParam, lParam);
	}
	return 0;
}


//REGISTER AND CREATE MAIN WINDOW
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{

	WNDCLASSEX wc;
	HWND hMainWindow;
	MSG Msg;

	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.style		 = 0;
	wc.lpfnWndProc	 = WndProc;
	wc.cbClsExtra	 = 100;
	wc.cbWndExtra	 = 100;
	wc.hInstance	 = hInstance;
	wc.hIcon		 = LoadImage(NULL, "Shazam.ico", IMAGE_ICON, 128, 128, LR_LOADFROMFILE);
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION);

	if(!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Window Registration Failed!", "Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	hMainWindow = CreateWindowEx(
		WS_EX_APPWINDOW,
		g_szClassName,
		"Stark::Language v1.5.3",
		WS_OVERLAPPEDWINDOW,
		0, 0, MAXIMUM_ALLOWED, MAXIMUM_ALLOWED,
		NULL, NULL, hInstance, NULL);

	if(hMainWindow == NULL)
	{
		MessageBox(NULL, "Window Creation Failed!", "Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	ShowWindow(hMainWindow, nCmdShow);
	UpdateWindow(hMainWindow);

	while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return Msg.wParam;
}

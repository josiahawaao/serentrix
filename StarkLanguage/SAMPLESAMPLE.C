#include<windows.h>

#define SZ_BITMAP "back.bmp"
#define SZ_INIT TEXT("Initializing ..")
#define SZ_LOAD TEXT("Loading resources... ")
#define SZ_CLOSE TEXT("Opening Stark::Language...")
#define SZ_SPLASH TEXT("\n                      Stark::Language\n                          version1.5.3")
#define ID_TIMER_CLOSE 0x1111
#define ID_TIMER_INIT 0x1112
#define ID_TIMER_LOAD 0x1113
#define ID_TIMER_DONE 0x1114

HBRUSH hBrush;
PAINTSTRUCT pStruct;
ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow);
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
HINSTANCE hInst = NULL;
TCHAR SplashWndClass[28];

    int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
    {

        MSG msg;
        lstrcpy(SplashWndClass,TEXT("SplashWindow"));
        MyRegisterClass(hInstance);
        // Perform application initialization:
        if (!InitInstance (hInstance, nCmdShow))
        {
            return FALSE;
        }
    // Main message loop:
        while (GetMessage(&msg, NULL, 0, 0))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    return msg.wParam;
    }
//
// FUNCTION: MyRegisterClass()
//
// PURPOSE: Registers the window class.
//
// COMMENTS:
//
// This function and its use is only necessary if you want
// this code to be compatible with Win32 systems prior to the
// 'RegisterClassEx' function that was added to Windows 95. It is
// important to call this function so that the application will
// get 'well formed' small icons associated with it.
    ATOM MyRegisterClass(HINSTANCE hInstance)
    {
        WNDCLASSEX wcex;

        wcex.cbSize = sizeof(WNDCLASSEX);
        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = (WNDPROC)WndProc;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = 0;
        wcex.hInstance = hInstance;
        wcex.hIcon = NULL;
        wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
        wcex.hbrBackground = (HBRUSH)(COLOR_BTNSHADOW);
        wcex.lpszMenuName = NULL;
        wcex.lpszClassName = SplashWndClass;
        wcex.hIconSm = NULL;


        HWND hWnd;
        hBrush = CreatePatternBrush((HBITMAP)LoadImage(NULL, "back.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE));

        DialogBox(hInstance,"MAIN", NULL,(DLGPROC)hWnd);
        return RegisterClassEx(&wcex);

    }
//
// FUNCTION: InitInstance(HANDLE, int)
// PURPOSE: Saves instance handle and creates main window
// In this function, we save the instance handle in a global
// variable and create and display the main program window.
//
    BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
    {
        HWND hWnd;
        RECT rect;
        int splashwidth = 700;
        int splashheight = 250;
        hInst = hInstance; // Store instance handle in this global variable
        SystemParametersInfo(SPI_GETWORKAREA, 0, (LPVOID) &rect, 0);
        hWnd = CreateWindowEx(WS_EX_CLIENTEDGE, SplashWndClass, NULL, WS_EX_CLIENTEDGE, (rect.right - rect.left - splashwidth)/2,
        (rect.bottom - rect.top - splashheight)/2, splashwidth, splashheight, NULL, NULL, hInstance, NULL);
        if (!hWnd)
        {
            return FALSE;
        }
            ShowWindow(hWnd, nCmdShow);
            UpdateWindow(hWnd);
        return TRUE;
    }
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
    switch (message)
    {
        case WM_NCCALCSIZE: //CAPTURE THIS MESSAGE AND RETURN NULL
            return 0;
        break;
        case WM_CREATE:
            SetTimer(hWnd, ID_TIMER_INIT, 1000, NULL);
            SetTimer(hWnd, ID_TIMER_LOAD, 2000, NULL);
            SetTimer(hWnd, ID_TIMER_DONE, 4000, NULL);
            SetTimer(hWnd, ID_TIMER_CLOSE, 5000, NULL);
        break;
        case WM_PAINT:
        {
            PAINTSTRUCT ps = { 0 };
            RECT rc = { 0 };
            HDC hDC = BeginPaint(hWnd, &ps);
            GetClientRect(hWnd, &rc);
            InflateRect(&rc, -2,-2);
            Rectangle(hDC, rc.left, rc.top, rc.right, rc.bottom);

            InflateRect(&rc, -15,-15);
           // HFONT hFont = CreateFont(35,35, 0, 0,0,0,0,
           // 0,0,0,0,0,0,TEXT("Century Gothic"));
           HFONT hFont2 = CreateFont(50,0,0,0,0,FALSE, FALSE, FALSE, 1,400, 0, 0, 0, ("Century Gothic"));
            HFONT hOldFont = (HFONT) SelectObject(hDC, hFont2);
            DrawText(hDC, SZ_SPLASH, lstrlen(SZ_SPLASH),
            &rc, DT_WORDBREAK);
            SelectObject(hDC, hOldFont);
            EndPaint(hWnd, &ps);

            if (GetUpdateRect(hWnd,0,0)) {
               BeginPaint(hWnd,&pStruct);
               if (wParam == NULL) FillRect(pStruct.hdc,&pStruct.rcPaint,hBrush);
               EndPaint(hWnd,&ps);
            }
        }
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        case WM_TIMER:
        {
            HDC hDC = GetDC(hWnd);
            RECT rc = { 0 };
            GetClientRect(hWnd, &rc);
            KillTimer(hWnd, wParam);
            switch (wParam)
            {
                case ID_TIMER_CLOSE:
                    DestroyWindow(hWnd);

                break;
                case ID_TIMER_INIT:
                    TextOut(hDC, rc.right-200, rc.bottom-20, SZ_INIT, lstrlen(SZ_INIT));
                break;
                case ID_TIMER_LOAD:
                    TextOut(hDC, rc.right-200, rc.bottom-20, SZ_LOAD, lstrlen(SZ_LOAD));
                break;
                case ID_TIMER_DONE:
                    TextOut(hDC, rc.right-200, rc.bottom-20, SZ_CLOSE, lstrlen(SZ_CLOSE));
                break;
            }
            ReleaseDC(hWnd, hDC);
        }
        break;
        default:
        return DefWindowProc(hWnd, message, wParam, lParam);
        }
    return 0;
    }
